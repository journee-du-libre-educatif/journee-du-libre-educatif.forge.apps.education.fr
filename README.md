Ce dossier contient le site de l'édition 2024 de la [Journée du Libre Éducatif 2024](https://journee-du-libre-educatif.forge.apps.education.fr).

La Journée du Libre Éducatif a pour objectif d’acculturer aux communs numériques et de soutenir l’écosystème de celles et ceux qui créent et partagent des logiciels et ressources éducatives libres utiles à la communauté scolaire.

La journée s’inscrit dans le « soutien au développement des communs numériques » de la Stratégie du numérique pour l’éducation 2023-2027 du ministère.

Ce site a été créé avec l'outil libre de création de site _Jekyll_, et le thème _jekyll-theme-conference_, également sous licence libre.

## Usage

Si nécessaire :

```
bundle exec gem update
bundle exec jekyll install
```

1/ Choisir la version de Ruby : 2.6.8

```
chruby 2.6.8
```
ou
```
rbenv version 2.6.8
```

2/ Lancer Jekyll

Première fois :
```
bundle jekyll install
```

Sinon :
```
bundle jekyll serve
```