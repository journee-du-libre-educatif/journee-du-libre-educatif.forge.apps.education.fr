---
layout: location
title: Localisation
---

[Lycée Carnot de Bruay-la-Buissière](https://www.lyceecarnot.fr/) (Pas-de-Calais).

51 rue Sadi Carnot,
62700 Bruay-la-Buissière

{% if site.conference.location.map %}
  <div id="map" class="mt-5 mb-5"></div>
{% endif %}

<style>
  img {max-height:350px}
  img.fslightbox-source {max-height:initial!important}
  p:last-of-type {margin-top:2em; margin-bottom:2em}
</style>

<script src="{{ site.baseurl }}/assets/js/fslightbox.js"></script>