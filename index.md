---
layout: home
---

![Affiche de présentation de la Journée du Libre Éducatif : le logo est à gauche en haut et on voit le lycée Bruay-La-Buissière où se déroule cette journée](assets/images/journee-libre-educatif-2025.png)

La Journée du Libre Éducatif a pour objectif d’acculturer aux communs numériques et de soutenir l’écosystème de celles et ceux qui créent et partagent des logiciels et ressources éducatives libres utiles à la communauté scolaire.

**« Pour un numérique inclusif, responsable et durable »**, tel sera le thème de la 4e édition de la **Journée du Libre Éducatif** qui se déroulera le **vendredi 4 avril 2025** au [lycée Carnot](https://www.lyceecarnot.fr/) de Bruay-la-Buissière (Pas-de-Calais).

S'inscrivant dans la [Stratégie du numérique pour l'éducation 2023-2027](https://www.education.gouv.fr/strategie-du-numerique-pour-l-education-2023-2027-344263), la JDLE a pour objectif d'acculturer un territoire aux **communs numériques** et de soutenir l'écosystème de celles et ceux qui créent et partagent des logiciels et ressources éducatives libres utiles à la communauté scolaire. Et de donner envie d'y participer !

## Organisation et inscription

Elle attend près de 250 participants pour 50 interventions (conférences, tables rondes, ateliers, stands, présentations flash).

Elle est co-organisée par le **lycée Carnot**, la **région académique des Hauts-de-France** et la **Direction du numérique pour l'éducation** du ministère de l'Éducation nationale.

Si vous appartenez à la région académique des Hauts-de-France, l'inscription à l'événement se fera prochainement sur les sites académiques de Lille et Amiens.

## Proposer une intervention

Ce formulaire vous invite à **soumettre une proposition d'intervention** : <https://framaforms.org/la-journee-du-libre-educatif-2025-1732711537>

D'une durée de **45 minutes**, les interventions se dérouleront l'après-midi entre 13h30 et 16h30, dans des salles de classe **de 30 à 50 places** du lycée, connectées et équipées en vidéoprojection. Il y aura a priori 3 sessions horaires en parallèle dans 6 salles soit **18 créneaux** possibles.

Ces interventions peuvent être de type frontales ou en ateliers. Les participants adultes de la Journée constitueront le public principal de vos ateliers mais il est également possible de **mobiliser les lycéens** de l'établissement si vous le souhaitez (à indiquer dans le descriptif).

**Date limite** : **dimanche 16 février 2025** à 23:59.

### Examen des propositions

Le comité de programme (composé de membres du lycée Carnot, de la DRANE Hauts-de-France et de la DNE) donnera son avis au plus tard le dimanche 23 février 2025.

Si vous êtes enseignant·e en exercice et que votre proposition est retenue, votre participation reste soumise à l'accord de votre direction d'établissement et de votre académie d'origine (n'hésitez pas à nous contacter en cas de difficulté).

Si votre proposition n'est pas retenue, il pourrait vous être proposé d'en faire une **présentation flash** en plénière le matin (5 min - 3 slides). Lorsque deux propositions sont proches, il est possible qu'on vous propose de les regrouper en co-intervention.

Pour tout complément d'information, contacter Alexis Kauffmann :  
alexis POINT kauffmann AT education POINT gouv POINT fr

Merci de votre participation !

## Les éditions précédentes

L'édition 2022 s'est déroulée [à Lyon](https://drane.ac-lyon.fr/spip/Journee-Du-Libre-Educatif-2022), celle de 2023 [à Rennes](https://journee-du-libre-educatif.forge.apps.education.fr/2023/) et celle de 2024 [à Créteil](https://journee-du-libre-educatif.forge.apps.education.fr/2024/). Vous pouvez consulter le [programme 2024](https://journee-du-libre-educatif.forge.apps.education.fr/2024/assets/pdf/JDLE2024-programme.pdf) pour vous faire une idée des projets présentés et ce **[clip-vidéo](https://podeduc.apps.education.fr/video/42285-journee-du-libre-educatif-a-luniversite-de-creteil/)** pour vous faire une idée de l'ambiance générale de l'événement.


<style>
	h2 {margin-top:1em; margin-bottom:0.5em}
	.jumbotron {background-color: #f0f5f2}
	.jumbotron > :last-child {margin-top:2em}
</style>