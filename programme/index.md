---
layout: program
---

Le programme n'est pas encore défini. N'hésitez pas à proposer une intervention !

<!-- <div class="alert alert-secondary alert-dismissible fade show mt-4 mb-5" role="alert">
Pensez à scroller sur la droite pour voir la suite du programme si votre écran n'affiche pas tout !
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>

Vous pouvez aussi [télécharger le programme](/assets/pdf/JDLE2024-programme.pdf) au format PDF. -->